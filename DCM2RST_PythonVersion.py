import dicom
import os
import gc

import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

import numpy as np

class DCM_reader(object):
    def __init__(self):
        dcm_path = ""
        self.data = {}

    def compare_dicom_key(self, dcm):
        """ Specifying the sorting key for CT images.
        """
        return float(dcm.ImagePositionPatient[2])

    def read_dicom_folder(self, path):
        """ Reads a folder with dicom files.
        Dicom object may be "CT", "RTSTRUCT", "RTDOSE" or "RTPLAN".
        """
        if not os.path.isdir(path):
            raise IOError("Folder does not exist")
        dcm_path = path
        # list of allowed dicom file extentions names
        # all in lower case
        dicom_suffix = ('.dcm', '.ima', '.v2')

        data = {}
        folder = os.listdir(path)
        for item in folder:
            if os.path.splitext(item)[1].lower() in dicom_suffix:
                dcm = dicom.read_file(os.path.join(path, item), force=True)
                if dcm.Modality == "CT":
                    if "images" not in data:
                        data["images"] = []
                    data["images"].append(dcm)
                elif dcm.Modality == "RTSTRUCT":
                    data["rtss"] = dcm
                elif dcm.Modality == "RTDOSE":
                    data["rtdose"] = dcm
                elif dcm.Modality == "RTPLAN":
                    if "rtplan" not in data:
                        data["rtplan"] = []
                    data["rtplan"].append(dcm)
        if "images" in data:
            data["images"].sort(key=self.compare_dicom_key)
        self.data = data
        
class RTplan(object):
    def __init__(self, dcm_reader, i):
        if not dcm_reader.data.has_key("rtplan"):
            print "RT plan was not found in dcm files!"
            return
        self.data = dcm_reader.data["rtplan"][i]
        self.pt_id = self.data["0x0010", "0x0020"].value
        self.beam_seq = self.data["0x300a", "0x03a2"].value
        self.beams = list()
        for beam in self.beam_seq:
            self.beams.append(RTbeam(beam))

def GetParticle(rt_type, ion_number):
    if "photon" in rt_type.lower():
        print "TRiP doesn't support photons, you monkey"
    elif "proton" in rt_type.lower():
        return "H"
    elif "ion" in rt_type.lower():
        return {
            2: "He",
            3: "Li",
            4: "Be",
            5: "B",
            6: "C",
            7: "N",
            8: "O",
            9: "F",
            10: "Ne",
            26: "Fe"
        }[ion_number]

class RTbeam(object):
    def __init__(self, beam):
        self.ion_mass = beam["0x300a", "0x0302"].value
        self.ion_number = beam["0x300a", "0x0304"].value
        self.ion_charge = beam["0x300a", "0x0306"].value
        self.particle = GetParticle(beam["0x300A","0x00C6"].value, self.ion_number)
        n_rng_shifters = beam["0x300a","0x0312"].value
        self.n_control_points = beam["0x300a", "0x0110"].value
        self.control_points = list()
        First = True
        for cp in beam["0x300a", "0x03a8"]:
            self.control_points.append(ControlPoint(cp, First, n_rng_shifters))
            if First:
                First = False

    def get_minmaxsum_particles(self):
        z = np.array(self.control_points[0].spot_meterset_w, dtype=np.float32)
        for cp in self.control_points:
            if cp.spot_meterset_w[0] < z[0]:
                z[0] = cp.spot_meterset_w[0]
            if cp.spot_meterset_w[1] > z[1]:
                z[1] = cp.spot_meterset_w[1]
            z[2] += cp.spot_meterset_w[0]
        self.p_min = z[0]
        self.p_max = z[1]
        self.p_sum = z[2]

class ControlPoint(object):
    def __init__(self, cp, first, n_rng_shifters):
        self.spot_size = cp["0x300a", "0x0398"].value
        self.control_point_idx = cp["0x300a", "0x0112"].value
        self.nom_beam_energy = cp["0x300a", "0x0114"].value
        self.cumsum_meterset_w = cp["0x300a", "0x0134"].value
        self.n_spots = cp["0x300a", "0x0392"].value
        self.spot_pos_map = cp["0x300a", "0x0394"].value
        self.spot_meterset_w = cp["0x300a", "0x0396"].value
        self.fwhm = cp["0x300a", "0x0398"].value # "Scanning spot size"
        self.n_rng_shifters = n_rng_shifters
        if first:
            self.gantry = cp["0x300a", "0x011E"].value
            self.couch = cp["0x300a", "0x0122"].value
            self.isocenter = cp["0x300a", "0x012c"].value
            self.isocenter[0] = -self.isocenter[0]
            self.isocenter[1] = -self.isocenter[1]
            self.rng_shift_list = [x["0x300a", "0x0366"].value for x in cp["0x300a", "0x360"]]

def RstWriter(dest_file, rt_plan):
    b = 0
    for beam in rt_plan.beams:
        b += 1
        gantry = beam.control_points[0].gantry
        array_iso = beam.control_points[0].isocenter
        iso_ctr = "_".join([str(d) for d in array_iso])
        dest_path = "%s_%d_iso(%s).rst" % (dest_file, b, iso_ctr)
        print "Guessing beam targets:"
        if abs(array_iso[2]) > 30 and (gantry < 10 or gantry > 350):
            print "Probably spine beam: %s" % dest_path
        elif abs(array_iso[2]) < 30:
            print "Probably brain beam: %s" % dest_path
        else:
            print "Unsure about this beam: %s" % dest_path
        sub_machs = list()
        beam.get_minmaxsum_particles()
        sub_machs.append(make_preamble(rt_plan.pt_id, gantry, beam.control_points[0].couch,
                                       beam.n_control_points, 
                                       [beam.p_min, beam.p_max, beam.p_sum],
                                       n_rng_shifters=beam.control_points[0].n_rng_shifters,
                                       mm_rng_list=[x for x in beam.control_points[0].rng_shift_list],
                                       ion=beam.particle,
                                       charge=beam.ion_charge,
                                       mass=beam.ion_mass))
        for cp in beam.control_points:
            sub_machs.append(add_sub_mach(cp, beam.n_control_points))
        output = "".join(sub_machs)
        with open(dest_path, "w") as fp:
            fp.write(output)


def make_preamble(pt_id_s="Agent_47", gantry_i=0, couch_i=-180, n_sub_machs_i=11, 
                  n_total_min_max_sum_l=[32994.6, 271402, 2.53098E+08], 
                  ripple_i=3, n_rng_shifters=0, mm_rng_list=[0],
                  ion="C", charge=6, mass=12):
    return ("rstfile 20010201\n" + # 20030630\n" +
            "sistable 19981218\n" +
            "patient_id %s\n" % pt_id_s +
            "machine# 0\n" +
            "projectile %s\n" % (ion) +
            "charge %d\n" % (charge) +
            "mass %d\n" % (mass) +
            "gantryangle %d\n" % (gantry_i - 90) +
            "couchangle %d\n" % (-couch_i - 90) +
            "stereotacticcoordinates\n" + 
            "bolus 0\n" +
            "ripplefilter %d\n" % ripple_i +
            ("shifter %s\n" % " ".join(format(int(x), "d") for x in mm_rng_list) if n_rng_shifters != 0 else "") +
            "#submachines %s\n" % n_sub_machs_i +
            "#particles %s\n" % " ".join(format(x, "f") for x in n_total_min_max_sum_l))

def add_sub_mach(cp, n_sub_machs):
    z = np.array(cp.spot_meterset_w, dtype=np.float32)
    spot_pos_vector = np.array(cp.spot_pos_map, dtype=np.float32)
    y = -spot_pos_vector[::2] # x<->y because TRiP is tripping on acid, I guess?
    x = spot_pos_vector[1::2]
    f = cp.fwhm                           # focus is the same as FWHM!
    e = cp.nom_beam_energy                # used to be cp.control_point_idx but 2. deg pol fit gave R-square of 1
    e_idx = 0.001465 * e * e + 0.1715 * e - 24.94
    points_str = "\n".join(["%d %d %f" % (x[i], y[i], z[i]) for i in range(len(z))]) # <xpos1 /mm > <ypos1 /mm > <#particles1>
    slices_str = ("slices %s\n" % " ".join("1" for x in range(cp.n_rng_shifters))) if cp.n_rng_shifters!=0 else ""
    return ("submachine# %d %.2f %d %.2f\n" % (e_idx, e, n_sub_machs, f[0]) + # <eindex1> <energy1> <findex1> <focus1>
            "#particles %f %f %f\n" %(z.min(), z.max(), z.sum()) + # <cutmin> <cutmax> <cutsum>
            "stepsize 1 1\n" + # <deltax/mm>  <deltay/mm> 
            slices_str +
            "#points %s\n" % (cp.n_spots) + # <n>
            points_str +
            "\n")

def main():
    wd = "/home/andreas/Shared/Laura/P6Cspine/"
    #wd = "C:/Users/Andreas/Desktop/P6Cspine/"
    plot_spots = False #True
    reader = DCM_reader();
    reader.read_dicom_folder(wd)
    for i in range(len(reader.data["rtplan"])):
        rt_plan = RTplan(reader, i)
        RstWriter("%s/RstOutput/test_%d" % (wd, i), rt_plan)
        gc.collect()

        if plot_spots:
            fig = plt.figure(i)
            b = 0
            for beam in rt_plan.beams:
                b += 1
                ax = fig.add_subplot(2, 3, b) #, projection='3d')
                first_cp = True
                for cp in beam.control_points:
                    if first_cp:
                        iso_cntr = cp.isocenter
                        print iso_cntr
                        first_cp = False
                    # spot_mtw_vector = np.array(cp.spot_meterset_w, dtype=np.float32)
                    spot_pos_vector = np.array(cp.spot_pos_map, dtype=np.float32)
                    x = spot_pos_vector[::2]
                    y = spot_pos_vector[1::2]
                    if plot_spots:
                        ax.scatter(x, y) #, spot_mtw_vector)
        if plot_spots:
            plt.show()

if __name__ == "__main__":
    main()
