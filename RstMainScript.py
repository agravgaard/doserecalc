"""Main function: Using PlanGenerator to create a plan for TRiP and execute it"""

import gc
import platform
import scipy.io as sio
import glob

import pytrip.dicomhelper as dh
from pytrip.tripexecuter import DoseCube
from MyPool import MyPool
from PlanGen import PlanGenerator
from TripExecuter import TripExecuter

MULTITHREADED = False
THREADS = 3

def main_by_angle(gantry, patient):
    """The main script, initiated in for loop over gantry parallel across
    patients"""
    plan_gen = PlanGenerator(int(patient), gantry)
    plan_name = "Test_14"
    plan_gen.set_tmp_dir("/home/andreas/tmp-%s" % (plan_name)) #patient, gantry))
    print platform.system().lower()
    if "cygwin" in platform.system().lower():
        work_dir = "/cygdrive/d/UserData/Dropbox/Laura"
    elif "linux" in platform.system().lower():
        work_dir = "/home/andreas/Shared/Laura"
    else:
        work_dir = "D:/UserData/Dropbox/Laura"
    plan_gen.set_work_dir(work_dir)
    plan_gen.set_result_dir("%s/results/" % work_dir)
    plan_gen.set_ct_dcm_dir("%s/P6Cspine/" % work_dir)
    plan_gen.set_rt_dcm_dir("%s/P6Cspine/" % work_dir)
    rst_files = glob.glob("%s/P6CSpine/RstOutput/test_%d_*" % (work_dir, int(patient) ) )
    for file in rst_files:
        #if int(file.split("_")[-4]) > 2:
        plan_gen.add_rst_file(file)
    
    # plan_gen.add_rst_file("%s/P6CSpine/RstOutput/test_0_3_iso(-3.62_-12.69_-139.67).rst" % work_dir)
    # plan_gen.add_rst_file("%s/P6CSpine/RstOutput/test_0_1_iso(3.62_13.99_-3).rst" % work_dir)
    # plan_gen.add_rst_file("%s/P6CSpine/RstOutput/test_0_2_iso(3.62_13.99_-3).rst" % work_dir)

    # Below is an example for adding VOIs to the plan using the name generator
    # You can also write the names directly ie.: target_name = "PTV 55/35"
    for name in plan_gen.vdx.get_voi_names():
        plan_gen.add_voi(name)
        print name
    target_name = "PTVspine1re" #plan_gen.voi_names.prostate_ptv
    ##Don't use with RST files: plan_gen.set_isocenter_from_voi(target_name)
    plan_gen.add_voi_as_target(target_name, 2)

    #plan_gen.add_voi("Lungs")
    #plan_gen.add_voi("PTVspine2")
    #plan_gen.add_voi("Thyroid")

    exe = TripExecuter(plan_gen.ctx)
    exe.execute(plan_gen.plan)
    print "Trip has completed calculation"

    # Below is the directory of the .dos and .hed files + the plan name
    plan_name = "%s/%s" % (exe.path, plan_gen.plan.get_name())
    dose_hed = "%s.phys.hed" % plan_name
    let_hed = "%s.dosemlet.hed" % plan_name


    #Write LVH's to result directory:
    plan_gen.plan.load_let(let_hed)
    let_dose = plan_gen.plan.get_let()
    print "Writing lvh and other stats..."
    plan_gen.write_dosemeanlet_data(let_dose)

    #Write DVH's to result directory:
    plan_gen.plan.load_dose(dose_hed, "phys", plan_gen.target_dose)
    phys_dose = plan_gen.plan.get_dose("phys")
    print "Writing dvh and other stats..."
    plan_gen.write_phys_dose_data(phys_dose)

    #Write .mat files for use with matlab:
    print "Writing let and dose .mat files..."
    sio.savemat("%s.let.mat" % plan_name, {"LETcube": let_dose.cube})
    sio.savemat("%s.phys.mat" % plan_name, {"PHYScube": phys_dose.dosecube.cube})
    print "Done with: %s" % plan_name


def main_by_patient(patient_idx):
    """For loop over angles, holds the patient list!"""
    patients = ['0','1','2']
    for gantry in [0]:
        print "Patient %s At angle %s Initiating..." % (patients[patient_idx],
                                                        gantry)
        main_by_angle(gantry, patients[patient_idx])

def main():
    """Parallel or sequential loop over patients"""
    patients = ['0','1','2']
    if not MULTITHREADED:
        map(main_by_patient, xrange(len(patients)))
    else:
        print "pre MyPool"
        pool = MyPool(THREADS)
        pool.map(main_by_patient, xrange(len(patients)))
        print "post map"
        pool.close()
        pool.join()

if __name__ == "__main__":
    main()
