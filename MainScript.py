"""Main function: Using PlanGenerator to create a plan for TRiP and execute it"""

import gc
import copy
# import platform
from multiprocessing import Pool

# import scipy.io as sio
# import pytrip.dicomhelper as dh
# from pytrip.tripexecuter import DoseCube
from pytrip.vdx import Voi, Slice, Contour

from PlanGen import PlanGenerator
from TripExecuter import TripExecuter
# import numpy as np
# from UtilityFunctions import display_slice_with_voi
# from UtilityFunctions import get_center_point

MULTI_THREADED = False
THREADS = 5
WRITE_LET = False
WRITE_DOSE = True


def get_work_dir():
    """Gets the system dependent work_dir"""
    work_dir = "/home/andreas/TRiP-Projects/HN_patients/"
    # work_dir = "/overdrive/HN_patients_TRIP/"
    return work_dir


def get_folder_maker(work_dir, data_sub_dir, two_lvls):
    """Get a function that only needs the modality specific argument to complete the path"""
    def maker(var):
        """currying in python"""
        return "/".join([work_dir, "data", data_sub_dir, var, two_lvls])
    return maker


# def make_sphere_voi(plan_gen):
#     """Creates a sphere voi, displays it (if flag set) and returns it along with its center"""
#     radius_mm = 25 # 5 cm diameter
#     center_point = get_center_point(plan_gen.ctx)
#     center_point = np.array([ # because offsets are destroyed in PyTRiP
#         center_point[0] - plan_gen.ctx.xoffset,
#         center_point[1] - plan_gen.ctx.yoffset,
#         center_point[2] - plan_gen.ctx.zoffset
#         ])
#     sphere_voi = create_sphere(plan_gen.ctx,
#                                "Sphere",
#                                center_point,
#                                radius_mm)
#
#     display_slice_with_voi(plan_gen.ctx, sphere_voi, center_point[2])
#     return sphere_voi, center_point


def write_dose_let_data(plan_name, plan_gen):
    """Get the trip output and write DVH, mean dose and stuff to files in result_dir"""

    if WRITE_LET:
        let_hed = "%s.dosemlet.hed" % plan_name
        # Write LVH's to result directory:
        plan_gen.plan.load_let(let_hed)
        let_dose = plan_gen.plan.get_let()
        print("Writing lvh and other stats...")
        plan_gen.write_dosemeanlet_data(let_dose)

    if WRITE_DOSE:
        dose_hed = "%s.phys.hed" % plan_name
        # Write DVH's to result directory:
        plan_gen.plan.load_dose(dose_hed, "phys", plan_gen.target_dose)
        phys_dose = plan_gen.plan.get_dose("phys")
        print("Writing dvh and other stats...")
        plan_gen.write_phys_dose_data(phys_dose)
        plan_gen.save_dose_as_dicom(phys_dose)
        print("Data was written.")

    return True


def get_ct_folders(work_dir, patient):
    """Assume a good naming scheme and generate a list of folders"""
    data_sub_dir = "Export_HNxx%s" % patient
    two_lvls = "PatientID_DCM/"
    ct_folders = ["AUTO_RIGID_CT", "COR_CBCT", "DEFORMED_CT_FINAL",
                  "RAW_CBCT", "MANUAL_RIGID_sCT", "MANUAL_RIGID_clinCBCT"]
    folder_curry = get_folder_maker(work_dir, data_sub_dir, two_lvls)
    ct_folders = [folder_curry(var) for var in ct_folders]
    return ct_folders


def get_offset_and_rs_from_file(plan_gen, work_dir, patient, modality):
    """Read the RS file into PlanGen and return the offset from move.txt
    modality is 'pCT' or 'sCT'"""
    data_dir = "%s/data/Export_HNxx%s/" % (work_dir, patient)
    plan_gen.set_rt_dcm_dir("%s/RS_%s/%s/" % (data_dir, patient, modality))
    mod_str = "_sCT" if modality == "sCT" else ""
    with open("%s/move%s.txt" % (data_dir, mod_str), "r") as fp:
        line = fp.readline().rstrip()
        line = line.replace("delta(mm):", "")
        line = line.replace(" ", "")
        offset = line.split(",")

    print("Offset: %s, %s, %s" % (offset[0], offset[1], offset[2]))
    return offset


def move_voi_by(voi, offset, prepend_id=""):
    """Loop through all contours in voi and shift them by offset"""
    new_voi = Voi(prepend_id + voi.name, voi.cube)
    new_voi.type = voi.type

    for slice_z in voi.slice_z:
        new_slice = Slice(voi.cube)
        for contour in voi.slices[slice_z].contour:
            for point in contour.contour:
                point[0] += float(offset[0])
                point[1] += float(offset[1])
                point[2] += float(offset[2])
            new_contour = Contour(contour.contour, voi.cube)
            new_slice.add_contour(new_contour)
        new_voi.add_slice(new_slice)

    return new_voi


def recalculate(plan_gen):
    """Recalculate plan saved in plan_gen on ct loaded in plan_gen"""
    offset = get_offset_and_rs_from_file(plan_gen, get_work_dir(), plan_gen.patient_number, "sCT")

    voi_list = []
    for voi_name in plan_gen.vdx.get_voi_names():
        voi = plan_gen.vdx.get_voi_by_name(voi_name)
        voi_list.append(move_voi_by(voi, offset, "moved_"))

    for voi in voi_list:
        plan_gen.vdx.add_voi(voi)
        if "moved_" in voi.name:
            plan_gen.add_voi(voi.name)

    exe = TripExecuter(plan_gen.ctx)
    exe.execute(plan_gen.plan)
    print("Trip has completed calculation")

    # Below is the directory of the .dos and .hed files + the plan name
    plan_name = "%s/%s" % (exe.path, plan_gen.plan.get_name())
    dose_hed = "%s.phys.hed" % plan_name

    if WRITE_LET:
        let_hed = "%s.dosemlet.hed" % plan_name
        # Write LVH's to result directory:
        plan_gen.plan.load_let(let_hed)
        let_dose = plan_gen.plan.get_let()
        print("Writing lvh and other stats...")
        plan_gen.write_dosemeanlet_data(let_dose)

    # Write DVH's to result directory:
    plan_gen.plan.load_dose(dose_hed, "phys", plan_gen.target_dose)
    phys_dose = plan_gen.plan.get_dose("phys")
    print("Writing dvh and other stats...")
    plan_gen.write_phys_dose_data(phys_dose)
    plan_gen.save_dose_as_dicom(phys_dose)
    print("Data was written.")


def main_by_angle(gantry, patient):
    """The main script, initiated in for loop over gantry parallel across
    patients"""
    plan_gen = PlanGenerator(int(patient), gantry)
    plan_name = "Test_0%s" % patient
    plan_gen.set_tmp_dir("/home/andreas/tmp-%s" % plan_name)
    work_dir = get_work_dir()
    plan_gen.set_work_dir(work_dir)

    ct_folders = get_ct_folders(work_dir, patient)

    plan_gen.set_result_dir("%s/results/" % work_dir)
    plan_gen.set_ct_dcm_dir(ct_folders[0])
    offset = get_offset_and_rs_from_file(plan_gen, work_dir, patient, "pCT")

    # plan_gen.generate_voi_name_dictionary()
    # plan_gen.voi_names.print_names()
    voi_list = []
    for voi_name in plan_gen.vdx.get_voi_names():
        voi = plan_gen.vdx.get_voi_by_name(voi_name)
        voi_list.append(move_voi_by(voi, offset, "moved_"))

    for voi in voi_list:
        plan_gen.vdx.add_voi(voi)
        if "moved_ITV" in voi.name:
            plan_gen.add_voi_as_target(voi.name, int(voi.name.split(" ")[1].split("/")[0]))
        elif "moved_" in voi.name:
            plan_gen.add_voi(voi.name)

    # plan_gen.add_voi_as_target("moved_ITV 60/BAW/HP")
    # plan_gen.add_voi_as_target("moved_ITV 50/BAW/HP")
    # plan_gen.set_isocenter_from_point(center_point)  # computed from taget if not set
    plan_gen.set_isocenter_from_voi("moved_ITV 66/BAW/HP")

    gc.collect()  # Destroy leftovers before calculating plan

    exe = TripExecuter(plan_gen.ctx)
    exe.execute(plan_gen.plan)
    print("Trip has completed calculation")

    # Below is the directory of the .dos and .hed files + the plan name
    plan_name = "%s/%s" % (exe.path, plan_gen.plan.get_name())

    if write_dose_let_data(plan_name, plan_gen):
        print("Results were written for %s" % plan_gen.plan.get_name())

    plan_gen_list = list()
    plan_gen.plan.set_optimize(False)
    for i, folder in enumerate(ct_folders):
        if i != 0:
            plan_gen_copy = copy.deepcopy(plan_gen)
            plan_gen_copy.set_ct_dcm_dir(folder)
            plan_gen_list.append(plan_gen_copy)

    if not MULTI_THREADED:
        map(recalculate, plan_gen_list)
    else:
        pool = Pool(THREADS)
        print("pre map")
        pool.map(recalculate, plan_gen_list)
        print("post map")
        pool.close()
        pool.join()

    print("Done with: %s" % plan_name)
    plan_gen.delete_tmp_dir()


def main_by_patient(patient_idx):
    """For loop over angles, holds the patient list!"""
    patients = ['1', '2', '3', '9']
    for gantry in range(0, 360, 5):
        print("Patient %s At angle %s Initiating..." % (patients[patient_idx],
                                                        gantry))
        main_by_angle(gantry, patients[patient_idx])


def main():
    """Parallel or sequential loop over patients"""
    patients = ['1', '2', '3', '9']
    map(main_by_patient, range(len(patients)))


if __name__ == "__main__":
    main()
