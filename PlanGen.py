"""Python module only containing one class: PlanGenerator"""

import os
import time
import shutil

from pytrip import CtxCube
from pytrip import VdxCube
import pytrip.dicomhelper as dh
from pytrip.tripexecuter import Voi
from pytrip.tripexecuter import Field

from tripplan import TripPlan

from numpy import int32 as npint32
from numpy import float64 as npfloat64
from numpy import array as nparray
from numpy import savetxt as npsavetxt

from Names import Names


class PlanGenerator(object):
    """A class that removes a lot of generic pytrip abstraction from the user"""
    def __init__(self, patient_number, gantry):
        self.tmp_dir = "/home/andreas/tmp"
        self.working_dir = "/home/andreas/Laura"
        self.result_dir = "/home/andreas/Laura/results"
        self.ct_type = "CT"
        self.ct_dcm_dir = ""
        self.rt_dcm_dir = ""
        self.time = time.clock()
        self.ctx = CtxCube()
        self.vdx = VdxCube("", self.ctx)
        self.voi_names = None
        self.voi_success = None
        self.voi_list = {}
        self.gantry = gantry
        self.couch = 0
        self.voxel_size = nparray([0, 0, 0])
        self.center = nparray([0, 0, 0])
        self.patient_number = patient_number
        # OPTIONS #
        self.side = "Left"  # LeFt, RiGhT or whatever if twofield
        self.twofield = False  # True
        self.margin = 3.0  # margin float in mm
        self.particle = "H"
        self.fwhm = 5
        self.rasterstep = 2
        self.target_dose = 2
        #############
        self.side_margin = "%s-%d" % (self.side, self.margin)
        self.plan = TripPlan(name="plan_P%d_g%d_%s" % (self.patient_number,
                                                       self.gantry,
                                                       self.side_margin))
        self.field = Field("field 1")
        self.field.set_projectile(self.particle)
        if self.twofield:
            self.field2 = Field("field 2")
            self.field2.set_projectile(self.particle)
        self.init_fields_and_plan()

    def init_fields_and_plan(self):
        """Function called by __init__ to set field and plan attributes"""
        self.plan.set_out_phys_dose(True)  # Doesn't really work with RstMain
        self.plan.set_out_mean_let(False)  # OPTIONAL
        # 90 degrees correspond to a field from the left side
        if 'left' in self.side.lower():
            self.field.set_gantry(self.gantry)
            self.field.set_couch(self.couch)
        elif self.twofield:
            self.field.set_gantry(self.gantry)
            self.field2.set_gantry(-self.gantry)
            self.field.set_couch(self.couch)
            self.field2.set_couch(-self.couch)
        else:
            self.field.set_gantry(-self.gantry)
            self.field.set_couch(-self.couch)
        self.field.set_fwhm(self.fwhm)
        self.field.set_rasterstep(self.rasterstep, self.rasterstep)
        if self.twofield:
            self.field2.set_fwhm(self.fwhm)
            self.field2.set_rasterstep(self.rasterstep, self.rasterstep)
        self.plan.set_out_field(True)
        if self.particle == "C":
            self.plan.set_sis_file('$TRIP98/DATA/SIS/12C.sis')
        elif self.particle == "H":
            self.plan.set_sis_file('$TRIP98/DATA/SIS/1H.sis')
        if self.target_dose > 2:
            self.plan.set_bio_algorithm('cl')  # ld also available for low dose
        else:
            self.plan.set_bio_algorithm('ld')

        if not self.twofield:
            self.plan.set_dose_algorithm('cl')  # cl, ms or ap also available
            self.plan.set_opt_princip('H2Obased')  # CTbased also available
        else:
            self.plan.set_dose_algorithm('ms')  # ms or ap also available
            self.plan.set_opt_princip('CTbased')
            self.plan.set_opt_algorithm('fr')  # gr, cg (classic) or bf also available

    def set_tmp_dir(self, path):
        """Set TRiPs temporary working directory directory"""
        self.tmp_dir = path
        self.plan.set_working_dir(path)

    def delete_tmp_dir(self):
        """Delete temporary TRiP directory directory"""
        shutil.rmtree(self.tmp_dir)

    def update_time(self):
        """Update the time for performance measurements"""
        self.time = time.clock()

    def set_work_dir(self, path):
        """Set working directory directory"""
        self.working_dir = path

    def set_result_dir(self, path):
        """Set result directory - MUST EXIST"""
        self.result_dir = path

    def set_ct_dcm_dir(self, path):
        """Set CT directory and read CT image"""
        self.ct_dcm_dir = path
        if "PatientID_DCM" in path:  # Remember sometimes extra "/PatientID_DCM"
            self.ct_type = path.split("PatientID_DCM")[0].split("/")[-2]
        print(path)
        print(self.ct_type)
        self.read_ct_dicom()

    def set_rt_dcm_dir(self, path):
        """Set Structure Dicom directory and read structures"""
        self.rt_dcm_dir = path
        self.read_rt_dicom()

    def read_ct_dicom(self, path=""):
        """Read CT image"""
        if path == "":
            path = self.ct_dcm_dir
        else:
            self.ct_dcm_dir = path
        print("CT-dir: %s" % path)
        self.ctx.read_dicom(dh.read_dicom_folder(path))
        self.ctx.cube = self.ctx.cube.astype(npint32)

    def read_rt_dicom(self, path=""):
        """Read structures"""
        if path == "":
            path = self.rt_dcm_dir
        else:
            self.rt_dcm_dir = path
        print("RT-dir: %s" % path)
        self.vdx = VdxCube("", self.ctx)
        dcm = dh.read_dicom_folder(path)
        self.vdx.read_dicom(dcm)

    def add_rst_file(self, path):
        """Add rst file to plan"""
        self.plan.add_rst_file(path)

    def generate_voi_name_dictionary(self):
        """Generate dictionary of VOI names look in Names.py for details"""
        self.voi_names = Names(self.vdx, self.patient_number, 0)
        self.voi_success = self.voi_names.auto_prostate()

    def add_fields(self):
        """Add fields (TRiP98 will compute isocenter)"""
        self.plan.add_field(self.field)
        if self.twofield:
            self.plan.add_field(self.field2)

    def set_isocenter_from_point(self, center_point):
        """Set Isocenter as center to be the given point (mm)"""
        self.field.set_target("%f,%f,%f" % (center_point[0],
                                            center_point[1],
                                            center_point[2]))
        self.plan.add_field(self.field)
        if self.twofield:
            self.field2.set_target("%f,%f,%f" % (center_point[0],
                                                 center_point[1],
                                                 center_point[2]))
            self.plan.add_field(self.field2)
        self.center = center_point

    def set_isocenter_from_voi(self, name):
        """Set Isocenter as center of the given VOI"""
        voi = self.vdx.get_voi_by_name(name)
        self.voxel_size = nparray([self.ctx.pixel_size,
                                   self.ctx.pixel_size,
                                   self.ctx.slice_distance])
        center_voxel = nparray([0.0, 0.0, 0.0], dtype=npfloat64)
        try:
            center_voxel = voi.calculate_center()
        except (LookupError, TypeError):
            print("you didn't pass a voi you fool!")
            try:
                center_voxel = voi.get_voi_data().calculate_center()
            except (LookupError, TypeError):
                print("you didn't even pass a Voi you dickhead!")
        self.field.set_target("%f,%f,%f" % (center_voxel[0],
                                            center_voxel[1],
                                            center_voxel[2]))
        self.plan.add_field(self.field)
        if self.twofield:
            self.field2.set_target("%f,%f,%f" % (center_voxel[0],
                                                 center_voxel[1],
                                                 center_voxel[2]))
            self.plan.add_field(self.field2)
        self.center = self.voxel_size / center_voxel

    def add_voi_as_target(self, name, dose=-1):
        """Add VOI to the plan as a target with the given dose as requirement"""
        if dose == -1:
            dose = self.target_dose
        voi = self.vdx.get_voi_by_name(name)
        fix_min_max(voi)
        self.voi_list[name] = voi
        self.plan.add_voi(Voi(name, voi))
        for voi_i in self.plan.get_vois():
            if voi_i.get_name() == name:
                voi_i.toogle_target()
                voi_i.set_dose(dose)

    def add_voi_as_oar(self, name, ratio=1.0):
        """Add VOI to the plan as a OAR with the given ratio being the fraction of
        the dose allowed as requirement"""
        voi = self.vdx.get_voi_by_name(name)
        fix_min_max(voi)
        self.voi_list[name] = voi
        self.plan.add_voi(Voi(name, voi))
        for voi_i in self.plan.get_vois():
            if voi_i.get_name() == name:
                voi_i.toogle_oar()
                voi_i.set_max_dose_fraction(ratio)

    def add_voi(self, name):
        """Add VOI by name to the list for calculation later"""
        voi = self.vdx.get_voi_by_name(name)
        fix_min_max(voi)
        self.voi_list[name] = voi

    def add_voi_by_voi(self, voi, name):
        """Fix Min/Max problem, then add the voi to the list. Both voi and name
        are required as input!"""
        fix_min_max(voi)
        self.voi_list[name] = voi

    def write_phys_dose_data(self, dose):
        """Create a file if it doesn't exist for writing min, max, mean dose and
        area of all VOI in the list after writing dvh of the VOI to a file"""
        if os.path.isfile("%sP%d_p%s_%s.txt" % (self.result_dir,
                                                self.patient_number, 0,
                                                self.side_margin)):
            file_p = open("%sP%s_p%s_%s.txt" % (self.result_dir,
                                                self.patient_number, 0,
                                                self.side_margin), "a")
        else:
            file_p = open("%sP%s_p%s_%s.txt" % (self.result_dir,
                                                self.patient_number, 0,
                                                self.side_margin), "w+")
            file_p.write("VOI\tGantry\tDmin\tDmax\tDmean\tArea\n")
        self.calc_dvh_and_write_stats(file_p, dose)
        file_p.close()

    def calc_dvh_and_write_stats(self, file_p, dose):
        """Write min, max, mean dose and area of all VOI in the list after
        writing dvh of the VOI to a file"""
        print("Writing dose data to disk...")
        for name, voi in self.voi_list.iteritems():
            print("Calculating dose data for %s..." % name)
            dvh, min_dose, max_dose, mean, area = dose.calculate_dvh(voi)
            data_string = "%s-%s-%s\t%d\t%d\t%d\t%d\t%d\n" % (self.side, name,
                                                              self.ct_type,
                                                              self.gantry,
                                                              min_dose, max_dose,
                                                              mean, area)
            print(data_string)
            file_p.write(data_string.encode("utf8"))
            name = name.replace(" ", "_").replace("/", "-")
            filename = '%s/%s/dvh_%s_%s_%s-%d-%d.txt' % (self.result_dir,
                                                         self.side,
                                                         self.ct_type,
                                                         name,
                                                         self.side_margin,
                                                         self.patient_number,
                                                         self.gantry)
            print("Saving dvh as %s..." % filename)
            npsavetxt(filename, dvh, delimiter=',')
        print("Done!")

    def save_dose_as_dicom(self, dos):
        """Write dose data to dicom files"""
        dos_cube = dos.get_dosecube()
        dcm = dos_cube.create_dicom()
        plan = dos_cube.create_dicom_plan()
        rtdose_filename = '%s/%s/rtdose_%s_%s-%d-%d.dcm' % (self.result_dir,
                                                            self.side,
                                                            self.ct_type,
                                                            self.side_margin,
                                                            self.patient_number,
                                                            self.gantry)

        rtplan_filename = '%s/%s/rtplan_%s_%s-%d-%d.dcm' % (self.result_dir,
                                                            self.side,
                                                            self.ct_type,
                                                            self.side_margin,
                                                            self.patient_number,
                                                            self.gantry)
        dcm.save_as(rtdose_filename)
        plan.save_as(rtplan_filename)

    def write_dosemeanlet_data(self, dose):
        """Create a file if it doesn't exist for writing min, max, mean dose and
        area"""
        if os.path.isfile("%sLVH_P%d_p%s_%s.txt" % (self.result_dir,
                                                    self.patient_number, 0,
                                                    self.side_margin)):
            file_p = open("%sLVH_P%s_p%s_%s.txt" % (self.result_dir,
                                                    self.patient_number, 0,
                                                    self.side_margin), "a")
        else:
            file_p = open("%sLVH_P%s_p%s_%s.txt" % (self.result_dir,
                                                    self.patient_number, 0,
                                                    self.side_margin), "w+")
            file_p.write("VOI\tGantry\tDmin\tDmax\tDmean\tArea\n")
        self.calc_lvh_and_write_stats(file_p, dose)
        file_p.close()

    def calc_lvh_and_write_stats(self, file_p, dose):
        """Write min, max, mean dose and area of all VOI in the list after writing
        lvh of the VOI to a file"""
        for name, voi in self.voi_list.iteritems():
            try:
                lvh, min_dose, max_dose, mean, area = dose.calculate_lvh(voi)
                file_p.write("%s-%s-%s\t%d\t%d\t%d\t%d\t%d\n" % (self.side, name, self.ct_type,
                                                                 self.gantry,
                                                                 min_dose, max_dose,
                                                                 mean, area))
                name = name.replace(" ", "_").replace("/", "-")
                npsavetxt('%s/%s/lvh_%s_%s_%s-%d-%d.txt' % (self.result_dir,
                                                            self.side,
                                                            self.ct_type,
                                                            name,
                                                            self.side_margin,
                                                            self.patient_number,
                                                            self.gantry),
                          lvh, delimiter=',')
            except IOError as ex_handle:
                print("%s of patient %d after dose!! IOError" % (name,
                                                                 self.patient_number))
                raise ex_handle
            except TypeError as ex_handle:
                print("%s of patient %d after dose!! TypeError" % (name,
                                                                   self.patient_number))
                raise ex_handle


def fix_min_max(voi):
    """If a slice is empty TRiP will fail, this can usually fix this issue.
    Look in Utilities for details"""
    tmp_bool = min_max_fixer(voi)
    if not tmp_bool:
        print("MinMaxFixer failed! expect %s to fail!!" % voi.get_name)


def min_max_fixer(voi):
    """Tries to find min and max if LookupError is raised delete the slice!"""
    boolean = True
    del_key = 0
    try:
        _, _ = voi.get_min_max()
    except LookupError:
        for key in voi.slices:
            try:
                _, _ = voi.slices[key].get_min_max()
            except LookupError:
                del_key = key

    if del_key != 0:
        del voi.slices[del_key]
        for idk, k in enumerate(voi.slice_z):
            if k == del_key:
                voi.slice_z.pop(idk)

    try:
        _, _ = voi.get_min_max()
    except LookupError:
        for key in voi.slices:
            try:
                _, _ = voi.slices[key].get_min_max()
            except LookupError:
                print("Still failing on key: %i, Please report errorkey!" % key)

    min_val, max_val = voi.get_min_max()
    try:
        print("Voi: %s, had min: [%d, %d, %d] and max: [%d, %d, %d]" % (voi.get_name(),
                                                                        min_val[0],
                                                                        min_val[1],
                                                                        min_val[2],
                                                                        max_val[0],
                                                                        max_val[1],
                                                                        max_val[2]))
    except LookupError:
        boolean = False
        print("Expect voi: %s to FAIL" % voi.get_name())

    return boolean


def split_voi(voi, xsplit, end):
    """ Deletes all points left xor right of xsplit.
    Input:
    voi:    voi class,
    xsplit: split position in x-coordinates
    end:    -1 is right 0 is left"""
    remove_key = []
    for key in voi.slices:
        remove_idi = []
        for i in range(len(voi.slices[key].contour)):
            split_voi_point_collector(key, i, voi,
                                      xsplit, end)
            if len(voi.slices[key].contour[i].contour) <= 1:
                remove_idi.append(i)
        remove_idi.sort(reverse=True)
        for k in remove_idi:
            voi.slices[key].contour.pop(k)
        try:
            _, _ = voi.slices[key].get_min_max()
        except LookupError:
            remove_key.append(key)
    remove_key.sort(reverse=True)
    for z_id in range(len(voi.slice_z) - 1, 0, -1):
        if voi.slice_z[z_id] in remove_key:
            del voi.slices[voi.slice_z[z_id]]
            voi.slice_z.pop(z_id)
    fix_min_max(voi)


def split_voi_point_collector(key, i, voi, xsplit, end):
    """Innermost loop of split_voi. Checks position of point and returns list of
    keys if left xor right of xsplit"""
    remove_id = []
    for j in range(len(voi.slices[key].contour[i].contour)):
        if end == -1:
            try:
                if xsplit < voi.slices[key].contour[i].contour[j][0]:
                    remove_id.append(j)
            except LookupError:
                print("contour(%i) failed to find index" % j)
        elif end == 0:
            try:
                if xsplit > voi.slices[key].contour[i].contour[j][0]:
                    remove_id.append(j)
            except LookupError:
                print("contour(%i) failed to find index" % j)
    remove_id.sort(reverse=True)
    for k in remove_id:
        voi.slices[key].contour[i].contour.pop(k)


def read_dose_dicom(dos, dcm):
    """Read dosecube from dicom file"""
    if dos.header_set is False:
        dos.read_dicom_header(dcm)
    dos.dimz = len(dcm["rtdose"].pixel_array)
    dos.dimx = len(dcm["rtdose"].pixel_array[0][:][0])
    dos.dimy = len(dcm["rtdose"].pixel_array[0][0][:])
    dos.read_dicom(dcm)
    return dos
